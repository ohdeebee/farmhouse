from PIL import Image, ImageFile

ALLOWED_IMG_EXTENSIONS = ['png', 'jpg', 'jpeg']


def file_is_image(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].casefold() in ALLOWED_IMG_EXTENSIONS


def compress_image(filepath):
    try:
        img = Image.open(filepath)

        if img.format.lower() in ALLOWED_IMG_EXTENSIONS:
            try:
                # This line avoids problems that can arise saving larger JPEG files with PIL
                ImageFile.MAXBLOCK = img.size[0] * img.size[1]

                # The 'quality' option is ignored for PNG files
                img.save(filepath, quality=35, optimize=True)
                return True
            except Exception as e:
                print("Image Processing/Saving Error: " + e.message)
                return False
        else:
            print("Invalid image format")
            return False
    except Exception as e:
        print("Image Opening Error: " + e.message)
        return False


