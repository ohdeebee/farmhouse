import flask
from flask import Flask, render_template
from flask_login import LoginManager
from models import db, User
from config import DevConfig, ProductionConfig

app = Flask(__name__, static_folder='static')
app.config.from_object(DevConfig)

# sql alchemy setup
# db = SQLAlchemy(app)
db.init_app(app)

# login manager
lm = LoginManager()
lm.init_app(app)

from views.admin import admin
from views.marketplace import marketplace

# setup blueprints
app.register_blueprint(admin, url_prefix='/backend')
app.register_blueprint(marketplace)


@app.errorhandler(Exception)
def handle_error(e):
    try:
        if e.code < 400:
            return flask.Response.force_type(e, flask.request.environ)
        elif e.code == 404:
            return render_template('marketplace/404.html', title="Page Not Found", msg="Sorry, but the page you were trying to view does not exist."), 404
        raise e
    except:
        return render_template('marketplace/404.html', title="Error", msg="Something went wrong"), 500
    # return render_template('marketplace/404.html'), 404

