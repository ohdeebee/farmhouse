from sqlalchemy import Column, Integer, DECIMAL, String, Date, DateTime, func, ForeignKey, Table, Text
from sqlalchemy.orm import relationship
from flask_sqlalchemy import SQLAlchemy
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from util.helpers import genID
from flask import Markup, current_app
import os

db = SQLAlchemy()

# association tables
farm_categories = Table('farm_categories', db.metadata,
                        Column('farm_id', Integer, ForeignKey('farm_profile.id')),
                        Column('category_id', Integer, ForeignKey('categories.id'))
                        )


class User(UserMixin, db.Model):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    full_name = Column(String(100))
    username = Column(String(50), unique=True)
    email = Column(String(120), unique=True)
    password_hash = Column(String(200))
    phone_number = Column(String(20))
    gender = Column(String(6))
    google_auth_id = Column(String(200))
    fb_auth_id = Column(String(200))
    sign_up_date = Column(DateTime, default=func.now())
    last_visited = Column(DateTime)
    user_type = Column(Integer, default=0)
    status = Column(Integer, default=0)

    farm = relationship("FarmProfile", uselist=False, back_populates="farmer", cascade="all, delete-orphan")
    wishlist = relationship("Wishlist", back_populates="owner", cascade="all, delete-orphan")
    reviews = relationship("Reviews", back_populates="reviewer", cascade="all, delete-orphan")
    notice_views = relationship("NoticeViews", uselist=False, cascade="all, delete-orphan")
    unread = relationship('Message', primaryjoin='and_(Message.receiver_id==User.id, Message.read_status==0)')
    verified = relationship('Verification', uselist=False, back_populates="user", cascade="all, delete-orphan")
    avatar = relationship("Avatar", uselist=False, back_populates="user")
    msgThreads = relationship('MessageThread', primaryjoin='or_(MessageThread.receiver_id==User.id, MessageThread.sender_id==User.id)')

    def __repr__(self):
        return self.full_name

    def __init__(self, full_name, username, email, password, phone, gender, user_type):
        self.full_name = full_name
        self.username = username.lower()
        self.email = email.lower()
        self.set_password(password)
        self.phone_number = phone
        self.gender = gender
        self.user_type = user_type

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def is_farmer(self):
        if self.user_type == 1:
            return True
        return False

    def is_admin(self):
        if self.user_type == 2:
            return True
        return False

    def is_verified(self):
        if self.verified is None:
            return False
        return True

    def has_avatar(self):
        if self.avatar is None:
            return False
        return True

    def has_email(self):
        if self.email is None:
            return False
        return True

    @property
    def get_avatar(self):
        if self.avatar:
            return '/static/' + current_app.config['UPLOAD_FOLDER'] + '/' + self.avatar.avatar_name
        else:
            return '/static/img/farmer.jpg'

    @property
    def unread_count(self):
        return len(self.unread)

    @property
    def new_notice(self):
        if not self.notice_views:
            return True
        else:
            # query Notices for max date and compare
            maxtime = db.session.query(func.max(Notices.date_posted).label("max_time")).one()
            if maxtime.max_time:
                if self.notice_views.date_viewed < maxtime.max_time:
                    return True
            return False


class FarmProfile(db.Model):
    __tablename__ = 'farm_profile'

    id = Column(Integer, primary_key=True)
    farm_name = Column(String(100))
    farm_profile = Column(Text)
    expertise = Column(String(100))
    years_active = Column(Integer)
    user_id = Column(Integer, ForeignKey('users.id'))
    location_state = Column(Integer, ForeignKey('states.id'))

    farmer = relationship("User", uselist=False, back_populates="farm")
    state = relationship("States", uselist=False)
    featured = relationship("FeaturedFarms", uselist=False, back_populates="farm",  cascade="all, delete-orphan")
    categories = relationship("Categories", secondary=farm_categories, back_populates="farms")
    reviews = relationship("Reviews", back_populates="farm", cascade="all, delete-orphan")
    ratings = relationship("Ratings", back_populates="farm", cascade="all, delete-orphan")
    products = relationship("Products", back_populates="farm", cascade="all, delete-orphan")

    def __repr__(self):
        return self.farm_name

    @property
    def is_featured(self):
        if self.featured:
            return True
        else:
            return False

    @property
    def product_count(self):
        return len(self.products)

    @property
    def get_rating(self):
        if self.ratings:
            x = 0
            total = 0
            for r in self.ratings:
                total = total+r.rating
                x=x+1
            return int(round(total/x))
        else:
            return False

    def get_formatted_rating(self):
        if self.ratings:
            max = self.get_rating
            retval = ''
            for x in range(0, 5):
                if max > 0:
                    retval += '<span><i class="fa fa-star" aria-hidden="true"></i></span>'
                    max -= 1
                else:
                    retval += '<span><i class="fa fa-star-o" aria-hidden="true"></i></span>'
        else:
            retval = ''
            for x in range(0, 5):
                retval += '<span><i class="fa fa-star-o" aria-hidden="true"></i></span>'
        return Markup(retval)


class FeaturedFarms(db.Model):
    __tablename__ = 'featured_farms'

    id = Column(Integer, primary_key=True)
    farm_id = Column(Integer, ForeignKey('farm_profile.id'))
    date_created = Column(DateTime, default=func.now())

    farm = relationship("FarmProfile", uselist=False, back_populates="featured")


class Verification(db.Model):
    __tablename__ = 'verification'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    date_created = Column(DateTime, default=func.now())

    user = relationship("User", uselist=False, back_populates="verified")


class Avatar(db.Model):
    __tablename__ = 'user_avatars'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    avatar_name = Column(String(100))
    date_created = Column(DateTime, default=func.now())

    user = relationship("User", uselist=False, back_populates="avatar")


class Products(db.Model):
    __tablename__ = 'products'

    id = Column(Integer, primary_key=True)
    product_name = Column(String(100))
    farm_id = Column(Integer, ForeignKey('farm_profile.id'))
    description = Column(Text)
    price = Column(DECIMAL(10, 2))
    measure_id = Column(Integer, ForeignKey('measures.id'))
    category_id = Column(Integer, ForeignKey('categories.id'))
    date_posted = Column(DateTime, default=func.now())

    category = relationship("Categories", uselist=False, back_populates="products")
    farm = relationship("FarmProfile", uselist=False, back_populates="products")
    featured = relationship("FeaturedProducts", uselist=False, back_populates="product",  cascade="all, delete-orphan")
    pictures = relationship("ProductPictures", back_populates="product", cascade="all, delete-orphan")
    measure = relationship("Measures", uselist=False)

    def __repr__(self):
        return self.product_name

    @property
    def is_featured(self):
        if self.featured:
            return True
        else:
            return False

    def has_pictures(self):
        if self.pictures:
            return True
        return False

    def picture_count(self):
        return len(self.pictures)

    def get_price(self):
        return str(self.price)+" per "+self.measure.measure

    def cover_picture(self):
        return self.pictures[0].picture_name


class FeaturedProducts(db.Model):
    __tablename__ = 'featured_products'

    id = Column(Integer, primary_key=True)
    product_id = Column(Integer, ForeignKey('products.id'))
    date_created = Column(DateTime, default=func.now())

    product = relationship("Products", uselist=False)


class Wishlist(db.Model):
    __tablename__ = 'wishlist'

    id = Column(Integer, primary_key=True)
    product_name = Column(String(100))
    description = Column(Text)
    preferred_location = Column(Integer, ForeignKey('states.id'))
    user_id = Column(Integer, ForeignKey('users.id'))
    status = Column(Integer, default=0)
    date_posted = Column(DateTime, default=func.now())
    date_updated = Column(DateTime)

    state = relationship("States", uselist=False)
    owner = relationship("User", uselist=False, back_populates="wishlist")

    def __repr__(self):
        return self.product_name


class Notices(db.Model):
    __tablename__ = 'notices'

    id = Column(Integer, primary_key=True)
    title = Column(String(100))
    notice = Column(Text)
    status = Column(Integer, default=0)
    date_posted = Column(DateTime, default=func.now())

    def __repr__(self):
        return self.title


class NoticeViews(db.Model):
    __tablename__ = 'notice_views'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    date_viewed = Column(DateTime, default=func.now())


class ProductPictures(db.Model):
    __tablename__ = 'product_pictures'

    id = Column(Integer, primary_key=True)
    picture_name = Column(String(100))
    product_id = Column(Integer, ForeignKey('products.id'))
    date_uploaded = Column(DateTime, default=func.now())

    def __repr__(self):
        return self.picture_name

    product = relationship("Products", uselist=False, back_populates="pictures")


class Reviews(db.Model):
    __tablename__ = 'reviews'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    farm_id = Column(Integer, ForeignKey('farm_profile.id'))
    review = Column(Text)
    date_posted = Column(DateTime, default=func.now())

    reviewer = relationship("User", uselist=False,  back_populates="reviews")
    farm = relationship("FarmProfile", uselist=False, back_populates="reviews")
    rating = relationship("Ratings", uselist=False)

    def get_formatted_rating(self):
        if self.rating.rating:
            max = self.rating.rating
            retval = ''
            for x in range(0, 5):
                if max > 0:
                    retval += '<span><i class="fa fa-star" aria-hidden="true"></i></span>'
                    max -= 1
                else:
                    retval += '<span><i class="fa fa-star-o" aria-hidden="true"></i></span>'
            return retval
        else:
            return False


class ReportUser(db.Model):
    __tablename__ = 'report_user'

    id = Column(Integer, primary_key=True)
    # reported user
    user_id = Column(Integer, ForeignKey('users.id'))
    complaint = Column(Text)
    # user making a report
    reporter_id = Column(Integer, ForeignKey('users.id'))
    # 0 for Pending, 1 for treated
    status = Column(Integer, default=0)
    date_posted = Column(DateTime, default=func.now())

    reported = relationship("User", uselist=False, foreign_keys=user_id)
    reported_by = relationship("User", uselist=False, foreign_keys=reporter_id)


class Ratings(db.Model):
    __tablename__ = 'ratings'

    id = Column(Integer, primary_key=True)
    review_id = Column(Integer, ForeignKey('reviews.id'))
    farm_id = Column(Integer, ForeignKey('farm_profile.id'))
    rating = Column(Integer)
    date_posted = Column(DateTime, default=func.now())

    farm = relationship("FarmProfile", uselist=False, back_populates="ratings")


class Categories(db.Model):
    __tablename__ = 'categories'

    id = Column(Integer, primary_key=True)
    category = Column(String(100))
    image = Column(String(100))

    farms = relationship("FarmProfile", secondary=farm_categories, back_populates="categories")
    products = relationship("Products", back_populates="category")

    def __repr__(self):
        return self.category

    @property
    def product_count(self):
        return len(self.products)


class States(db.Model):
    __tablename__ = 'states'

    id = Column(Integer, primary_key=True)
    state = Column(String(30))

    def __repr__(self):
        return self.state


class Measures(db.Model):
    __tablename__ = 'measures'

    id = Column(Integer, primary_key=True)
    measure = Column(String(50))

    def __repr__(self):
        return self.measure


class MessageThread(db.Model):
    __tablename__ = 'message_threads'

    id = Column(Integer, primary_key=True)
    urid = Column(String(200), unique=True)
    sender_id = Column(Integer, ForeignKey('users.id'))
    receiver_id = Column(Integer, ForeignKey('users.id'))
    date_created = Column(DateTime, default=func.now())

    sender = relationship('User', uselist=False, foreign_keys=sender_id)
    receiver = relationship('User', uselist=False, foreign_keys=receiver_id)
    msgs = relationship('Message', foreign_keys='Message.thread_id', back_populates='thread',
                           order_by='Message.date_created')

    def __repr__(self):
        return '<MessageThreadId %r>' % self.id

    def __init__(self, sender, receiver):
        self.sender_id = sender
        self.receiver_id = receiver
        self.urid = genID(MessageThread, MessageThread.urid)

    def get_last_msg(self):
        return self.msgs[-1]


class Message(db.Model):
    __tablename__ = 'messages'

    id = Column(Integer, primary_key=True)
    thread_id = Column(Integer, ForeignKey('message_threads.id'))
    sender_id = Column(Integer, ForeignKey('users.id'))
    receiver_id = Column(Integer, ForeignKey('users.id'))
    message = Column(String(4000))
    date_created = Column(DateTime, default=func.now())
    date_read = Column(DateTime)
    read_status = Column(Integer, default=0)

    thread = relationship('MessageThread', foreign_keys=thread_id, back_populates='msgs')
    sender = relationship('User', foreign_keys=sender_id)
    receiver = relationship('User', foreign_keys=receiver_id)

    def __repr__(self):
        return self.message

    def __init__(self, message, thread, sender, receiver):
        self.message = message
        self.thread_id = thread
        self.sender_id = sender
        self.receiver_id = receiver

    @property
    def is_read(self):
        if self.read_status == 1:
            return True
        return False


class AuditTrail(db.Model):
    __tablename__ = 'audit_trail'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    type = Column(String(200))
    description = Column(String(500))
    date_created = Column(DateTime, default=func.now())

    user = relationship("User", uselist=False)