# Configuration module
import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    """
    Base configuration object from which subclasses will inherit

    """

    DEBUG = False
    SECRET_KEY = 'ace2376bde74521abdf612345'
    SQLALCHEMY_DATABASE_URI = os.environ['FARM_DATABASE_URL']
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    UPLOAD_FOLDER = 'uploads'
    MAX_PICTURE_COUNT = 4
    PRODUCTS_PER_PAGE = 10
    FEATURED_FARM_COUNT = 3
    FEATURED_PRODUCT_COUNT = 4


class DevConfig(Config):
    """
    Development config object

    """

    DEBUG = True
    DEVELOPMENT = True
    EXPLAIN_TEMPLATE_LOADING = False
    SEND_EMAIL = False
    SERVER_NAME = 'theagricstore.local:5000'


class ProductionConfig(Config):
    """
    Production config object

    """

    DEBUG = False
    TRAP_HTTP_EXCEPTIONS = True
    SERVER_NAME = 'theagricstore.com'
