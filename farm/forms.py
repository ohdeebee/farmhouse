from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SelectField, widgets, SelectMultipleField, \
    DecimalField, TextAreaField, IntegerField, RadioField
from wtforms.validators import DataRequired, Email, EqualTo, Length, Optional
from util.validators import Unique, Numeric, RequiredIf
from flask_wtf.file import FileField, FileAllowed, FileRequired
from models import User, Categories, Measures

genderOptions = [('Male', 'Male'), ('Female', 'Female')]
userOptions = [('0', 'Buyer'), ('1', 'Farmer')]


class MultiCheckboxField(SelectMultipleField):
    """
    A multiple-select, except displays a list of checkboxes.

    Iterating the field will produce subfields, allowing custom rendering of
    the enclosed checkbox fields.
    """
    widget = widgets.ListWidget(prefix_label=False)
    option_widget = widgets.CheckboxInput()


class LoginForm(FlaskForm):
    username = StringField('Username/Email', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])


class SignupForm(FlaskForm):
    full_name = StringField('Full Name', validators=[DataRequired(), Length(max=100)])
    username = StringField('Username', validators=[DataRequired(), Length(min=2, max=50), Unique(User, User.username, 'This username already exists')])
    email = StringField('Email Address', validators=[Optional(), Length(min=6, max=120), Email(), Unique(User, User.email, 'This email already exists')])
    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password', [DataRequired(),
                                                          EqualTo('password', 'The passwords do not match')])
    phone = StringField('Phone Number', [DataRequired(), Length(max=13),
                                         Numeric(User, User.phone_number, message='Your phone number must be numeric'),
                                         Unique(User, User.phone_number, message='This phone number already exists.')])
    gender = SelectField('Gender', [DataRequired()], choices=genderOptions, default='Male')
    user_type = SelectField('Account Type', [DataRequired()], choices=userOptions, default=0)
    # farm signup fields
    farm_name = StringField('Farm Name', validators=[RequiredIf(user_type=1), Length(min=2)])
    farm_profile = TextAreaField('Farm Profile', validators=[RequiredIf(user_type=1), Length(min=3, max=2000)])
    expertise = StringField('Area of Expertise', validators=[RequiredIf(user_type=1), Length(max=100)])
    years_active = StringField('Years Active', validators=[RequiredIf(user_type=1)])
    state = SelectField('State', coerce=int, validators=[RequiredIf(user_type=1)])
    categories = MultiCheckboxField('Categories', coerce=int)


class EditProfileForm(FlaskForm):
    full_name = StringField('Full Name', validators=[DataRequired(), Length(max=100)])
    username = StringField('Username', validators=[DataRequired(), Length(min=2, max=50)])
    email = StringField('Email Address', validators=[Optional(), Length(min=6, max=120), Email()])
    phone = StringField('Phone Number', [DataRequired(), Length(max=13),
                                         Numeric(User, User.phone_number, message='Your phone number must be numeric')])
    gender = SelectField('Gender', [DataRequired()], choices=genderOptions)
    user_type = SelectField('Account Type', [DataRequired()], choices=userOptions)

    # farm fields
    farm_name = StringField('Farm Name', validators=[RequiredIf(user_type=1), Length(min=2)])
    farm_profile = TextAreaField('Farm Profile', validators=[RequiredIf(user_type=1), Length(min=3, max=2000)])
    expertise = StringField('Area of Expertise', validators=[RequiredIf(user_type=1), Length(max=100)])
    years_active = StringField('Years Active', validators=[RequiredIf(user_type=1)])
    state = SelectField('State', coerce=int, validators=[RequiredIf(user_type=1)])
    categories = MultiCheckboxField('Categories', coerce=int)


class ReviewForm(FlaskForm):
    review = TextAreaField('Review', validators=[DataRequired(), Length(min=3, max=2000)])
    rating = IntegerField('Rating', validators=[DataRequired()])


class ReportForm(FlaskForm):
    user_id = IntegerField('Report User')
    complaint = TextAreaField('Complaint', validators=[DataRequired(), Length(min=3, max=2000)])


class AddWishlistForm(FlaskForm):
    product_name = StringField('Product Name', validators=[DataRequired(), Length(min=2, max=100)])
    description = TextAreaField('Description', validators=[DataRequired(), Length(min=3, max=2000)])
    preferred_location = SelectField('Preferred Location', coerce=int, validators=[DataRequired()])


class AddProductForm(FlaskForm):
    product_name = StringField('Product Name', validators=[DataRequired(), Length(min=3)])
    description = TextAreaField('Description', validators=[DataRequired(), Length(min=3, max=2000)])
    price = DecimalField('Price', validators=[DataRequired()])
    measure = SelectField('', coerce=int, validators=[DataRequired()])
    category = SelectField('Categories', coerce=int)
    picture1 = FileField('Pictures', validators=[FileAllowed(['jpg', 'jpeg', 'png'], 'Images must be in either jpeg or png formats!')])
    picture2 = FileField('Pictures', validators=[FileAllowed(['jpg', 'jpeg', 'png'], 'Images must be in either jpeg or png formats!')])
    picture3 = FileField('Pictures', validators=[FileAllowed(['jpg', 'jpeg', 'png'], 'Images must be in either jpeg or png formats!')])
    picture4 = FileField('Pictures', validators=[FileAllowed(['jpg', 'jpeg', 'png'], 'Images must be in either jpeg or png formats!')])


class MessageForm(FlaskForm):
    message = TextAreaField('Message', validators=[DataRequired()])


class ResetPasswordForm(FlaskForm):
    email = StringField('Email Address', validators=[DataRequired(), Length(min=6, max=120), Email()])


class UploadAvatarForm(FlaskForm):
    avatar = FileField('Pictures', validators=[DataRequired(), FileAllowed(['jpg', 'jpeg', 'png'], 'Images must be in either jpeg or png formats!')])


class ChangePasswordForm(FlaskForm):
    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password', [DataRequired(),
                                                          EqualTo('password', 'The passwords do not match')])


class SearchProductForm(FlaskForm):
    keyword = StringField('Search')
    state = SelectField('State', coerce=int)
    sort = IntegerField('Sort')
    categories = MultiCheckboxField('Categories', coerce=int)


# Forms for the admin interface
class AddAdminForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired(), Length(min=2, max=50), Unique(User, User.username, 'This username already exists')])
    full_name = StringField('Full Name', validators=[DataRequired(), Length(max=100)])
    email = StringField('Email Address', validators=[DataRequired(), Length(min=6, max=120), Email(), Unique(User, User.email, 'This email already exists')])
    phone = StringField('Phone Number', [DataRequired(), Length(max=13),
                                         Numeric(User, User.phone_number, message='Your phone number must be numeric'),
                                         Unique(User, User.phone_number, message='This phone number already exists.')])
    gender = SelectField('Gender', [DataRequired()], choices=genderOptions, default='Male')
    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password', [DataRequired(),
                                                          EqualTo('password', 'The passwords do not match')])


class AddPostForm(FlaskForm):
    title = StringField('Post Title', validators=[DataRequired(), Length(min=2, max=100)])
    notice = TextAreaField('Post', validators=[DataRequired()])


class CategoryForm(FlaskForm):
    category = StringField('Category Name', validators=[DataRequired(), Length(min=2, max=100), Unique(Categories, Categories.category, message='This category already exists.')])
    category_icon = StringField('Category Icon', validators=[DataRequired(), Length(min=2, max=100)])


class MeasureForm(FlaskForm):
    measure = StringField('Measure', validators=[DataRequired(), Unique(Measures, Measures.measure, message='This name already exists.')])