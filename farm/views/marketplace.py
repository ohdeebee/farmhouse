import re
import os
import uuid
from flask import Blueprint, render_template, abort, request, flash, redirect, url_for, current_app
from flask_login import login_user, logout_user, login_required, current_user
from sqlalchemy import func, and_, or_
from functools import wraps
from jinja2 import TemplateNotFound
from werkzeug.utils import secure_filename
from farm import lm
from farm.models import User, db, States, Categories, FarmProfile, Products, ProductPictures, Measures, \
    Message, MessageThread, Wishlist, Notices, NoticeViews, Avatar, Reviews, Ratings, ReportUser
from farm.forms import LoginForm, SignupForm, AddProductForm, AddWishlistForm, \
    MessageForm, ResetPasswordForm, ChangePasswordForm, SearchProductForm, UploadAvatarForm, ReviewForm, \
    ReportForm, EditProfileForm
from farm.util.image_util import compress_image
from farm.util.helpers import randompwd

marketplace = Blueprint('marketplace', __name__)
lm.login_view = '.login'
lm.blueprint_login_views = '.login'

# decorator to determine if user owns a farm
def is_farmer(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not current_user.is_farmer():
            abort(403)
        return f(*args, **kwargs)
    return decorated_function


# login manager user loader
@lm.user_loader
def load_user(userid):
    return User.query.filter(User.id == userid).first_or_404()


# remove uploaded files
def remove_file(file_name):
    os.remove(os.path.join(current_app.static_folder, current_app.config['UPLOAD_FOLDER'], file_name))


@marketplace.route('/', methods=['GET'])
@marketplace.route('/index', methods=['GET'])
def index():
    # get states
    states = States.query.all()
    # get categories
    cats = Categories.query.limit(12).all()
    try:
        return render_template('marketplace/index.html', cats=cats, states=states)
    except TemplateNotFound:
        abort(404)


@marketplace.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('.home'))
    form = LoginForm()

    if form.validate_on_submit():
        username = form.username.data

        if re.match(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)", username):
            user = User.query.filter_by(email=username.lower()).first()
        else:
            user = User.query.filter_by(username=username.lower()).one_or_none()
            if user is None:
                user = User.query.filter_by(phone_number=username.lower()).one_or_none()

        if user is None:
            flash('Invalid login credentials', 'error')
        else:
            if user.check_password(form.password.data):
                # check user status
                if user.status == 0:
                    # login user
                    login_user(user)
                    # update user
                    user.last_visited = func.now()
                    db.session.add(user)
                    db.session.commit()
                    return redirect(url_for('.home'))
                else:
                    flash('This account has been deactivated. Kindly send an email to admin@theagricstore.com for help', 'error')
            else:
                flash('Invalid login credentials', 'error')
    try:
        return render_template('marketplace/login.html', form=form)
    except TemplateNotFound:
        abort(404)


@marketplace.route('/reset-password', methods=['GET', 'POST'])
def reset_password():
    if current_user.is_authenticated:
        return redirect(url_for('.home'))
    form = ResetPasswordForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).one_or_none()

        if user is None:
            flash('This email address has not been registered on this website', category='error')
        else:
            # reset password
            pwd = randompwd()
            user.set_password(pwd)
            db.session.add(user)
            db.session.commit()
            flash('Your password has been successfully reset. Your new password has been sent to your email', category='success')

            # check whether to send email
            if current_app.config['SEND_EMAIL']:
                pass
            else:
                print('new password: ' + pwd)

    return render_template('marketplace/reset_pwd.html', form=form)


@marketplace.route('/change-password', methods=['GET', 'POST'])
@login_required
def change_password():
    if not current_user.is_authenticated:
        return redirect(url_for('.login'))
    form = ChangePasswordForm()

    if form.validate_on_submit():
        pwd = form.password.data
        current_user.set_password(pwd)
        db.session.add(current_user)
        db.session.commit()

        flash('Your password has been successfully changed.')

    return render_template('marketplace/change_pwd.html', form=form)


@marketplace.route('/upload-avatar', methods=['GET', 'POST'])
@login_required
def upload_avatar():
    form = UploadAvatarForm()

    if form.validate_on_submit():
        # check if user has avatar
        if current_user.has_avatar():
            remove_file(current_user.avatar.avatar_name)
            db.session.delete(current_user.avatar)
            db.session.commit()

        img = request.files['avatar']
        file_name = str(uuid.uuid4())[0:10] + secure_filename(img.filename).lower()
        image_file = os.path.join(current_app.static_folder, current_app.config['UPLOAD_FOLDER'], file_name)
        img.save(image_file)
        compress_image(image_file)

        avatar = Avatar()
        avatar.user_id = current_user.id
        avatar.avatar_name = file_name
        db.session.add(avatar)
        db.session.commit()

        flash('Your avatar has been updated.', 'success')

    return render_template('marketplace/upload_avatar.html', form=form)


# update last visited and logout user
@marketplace.route('/logout', methods=['GET'])
@login_required
def logout():
    if not current_user.is_authenticated:
        return redirect(url_for('.login'))

    current_user.last_visited = func.now()
    db.session.add(current_user)
    db.session.commit()
    logout_user()
    flash('You have been logged out. Come back soon', 'success')
    return redirect(url_for('.index'))


@marketplace.route('/signup', methods=['GET', 'POST'])
def signup():
    if current_user.is_authenticated:
        return redirect(url_for('.home'))

    form = SignupForm()
    form.state.choices = [(a.id, a.state) for a in States.query.filter(States.id != 1).all()]
    form.categories.choices = [(a.id, a.category) for a in Categories.query.order_by('category')]

    if form.validate_on_submit():
        user = User(form.full_name.data.strip(), form.username.data.strip(), form.email.data.strip(), form.password.data, form.phone.data.strip(),
                    form.gender.data, form.user_type.data)
        db.session.add(user)
        db.session.commit()

        if user.user_type == 1:
            farm = FarmProfile()
            farm.farm_name = form.farm_name.data
            farm.farm_profile = form.farm_profile.data
            farm.expertise = form.expertise.data
            farm.years_active = form.years_active.data
            farm.location_state = form.state.data
            farm.user_id = user.id

            cats = Categories.query.filter(Categories.id.in_(form.categories.data))
            farm.categories.extend(cats)

            db.session.add(farm)
            db.session.commit()

        # login user
        login_user(user)
        # update user
        user.last_visited = func.now()
        db.session.add(user)
        db.session.commit()

        # set flash message
        flash('Welcome to the theagricstore.', 'success')
        return redirect(url_for('.home'))
    try:
        return render_template('marketplace/signup.html', form=form)
    except TemplateNotFound:
        abort(404)


@marketplace.route('/home', methods=['GET'])
@login_required
def home():
    try:
        return render_template('marketplace/home.html')
    except TemplateNotFound:
        abort(404)


@marketplace.route('/profile/<int:userid>', methods=['GET', 'POST'])
@login_required
def profile(userid):
    if userid == current_user.id:
        return redirect(url_for('.home'))
    user = load_user(userid)
    form = ReviewForm()

    if form.validate_on_submit():
        rev = Reviews.query.filter(Reviews.farm_id==user.farm.id, Reviews.user_id==current_user.id).one_or_none()

        if not rev:
            review = Reviews()
            review.farm_id = user.farm.id
            review.user_id = current_user.id
            review.review = form.review.data
            db.session.add(review)
            db.session.commit()

            #for rating
            rating = Ratings()
            rating.farm_id = user.farm.id
            rating.review_id = review.id
            rating.rating = form.rating.data

            db.session.add(rating)
            db.session.commit()

            flash('Your review has been submitted.', 'success')
        else:
            flash('You have already reviewed this farm.', 'error')
    # check if user has reviewed form before
    rev = Reviews.query.filter(Reviews.farm_id==user.farm.id, Reviews.user_id==current_user.id).one_or_none()
    if rev:
        review_check = True
    else:
        review_check = False

    try:
        return render_template('marketplace/profile.html', user=user, form=form, review_check=review_check)
    except TemplateNotFound:
        abort(404)

@marketplace.route('/profile/products', methods=['GET'])
@login_required
def profile_products():
    try:
        return render_template('marketplace/profile_products.html')
    except TemplateNotFound:
        abort(404)

@marketplace.route('/profile/message', methods=['GET'])
@login_required
def profile_message():
    try:
        return render_template('marketplace/profile_message.html')
    except TemplateNotFound:
        abort(404)


@marketplace.route('/profile/rating', methods=['GET'])
@login_required
def profile_rating():
    try:
        return render_template('marketplace/profile_rating.html')
    except TemplateNotFound:
        abort(404)


@marketplace.route('/report-user/<int:user_id>', methods=['GET', 'POST'])
@login_required
def report_user(user_id):
    user = load_user(user_id)
    if not user:
        abort(404)
    form = ReportForm()

    if form.validate_on_submit():
        report = ReportUser()
        report.user_id = form.user_id.data
        report.complaint = form.complaint.data
        report.reporter_id = current_user.id

        db.session.add(report)
        db.session.commit()

        flash('Your complaint has been successfully submitted', 'success')
    try:
        return render_template('marketplace/report_user.html', user=user, form=form)
    except TemplateNotFound:
        abort(404)


@marketplace.route('/edit-profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm(obj=current_user)
    states = States.query.filter(States.id != 1).all()
    cats = Categories.query.all()

    if form.validate_on_submit():
        # populate user objects
        current_user.full_name = form.full_name.data
        current_user.username = form.username.data
        current_user.email = form.email.data
        current_user.phone_number = form.phone.data
        current_user.gender = form.gender.data
        db.session.add(current_user)

        if current_user.user_type == 1:
            # check if user has a farm
            if current_user.farm:
                farm = FarmProfile.query.filter(FarmProfile.user_id==current_user.id).one_or_none()
            else:
                farm = FarmProfile()
                farm.user_id = current_user.id

            farm.farm_name = form.farm_name.data
            farm.farm_profile = form.farm_profile.data
            farm.expertise = form.expertise.data
            farm.years_active = form.years_active.data
            farm.location_state = form.state.data

            cats = Categories.query.filter(Categories.id.in_(form.categories.data))
            farm.categories.extend(cats)

            db.session.add(farm)
        db.session.commit()
        flash('Your profile has been successfully updated', 'success')
        return redirect(url_for('.home'))
    try:
        return render_template('marketplace/profile-edit.html', form=form, states=states, cats=cats)
    except TemplateNotFound:
        abort(404)


@marketplace.route('/my-products', methods=['GET'])
@login_required
@is_farmer
def my_products():
    try:
        return render_template('marketplace/my_products.html')
    except TemplateNotFound:
        abort(404)


@marketplace.route('/add-product', methods=['GET', 'POST'])
@login_required
@is_farmer
def add_product():
    form = AddProductForm()
    form.category.choices = [(a.id, a.category) for a in Categories.query.order_by('category')]
    form.measure.choices = [(a.id, a.measure) for a in Measures.query.order_by('measure')]

    if form.validate_on_submit():
        product = Products()
        product.product_name = form.product_name.data
        product.farm_id = current_user.farm.id
        product.description = form.description.data
        product.price = form.price.data
        product.measure_id = form.measure.data
        product.category_id = form.category.data

        db.session.add(product)
        images = request.files.iteritems()

        if images:
            for p in images:
                img = p[1]
                # Create Images
                if img.filename:
                    file_name = str(uuid.uuid4())[0:10] + secure_filename(img.filename)
                    image_file = os.path.join(current_app.static_folder, current_app.config['UPLOAD_FOLDER'], file_name)
                    img.save(image_file)
                    compress_image(image_file)

                    pic = ProductPictures()
                    pic.picture_name = file_name
                    product.pictures.append(pic)
                    db.session.add(product)

        db.session.commit()

        flash('Your product has been created.', 'success')
        return redirect(url_for('.my_products'))
    try:
        return render_template('marketplace/add_product.html', form=form)
    except TemplateNotFound:
        abort(404)


@marketplace.route('/view-product/<int:prod_id>', methods=['GET', 'POST'])
def view_product(prod_id):
    # get product
    prod = Products.query.filter(Products.id == prod_id).one_or_none()

    if prod is None:
        flash('This product does not exist', 'error')
        return redirect(url_for('.products'))

    #get related products
    related = Products.query.filter(Products.farm_id == prod.farm_id).limit(2).all()

    try:
        return render_template('marketplace/view_product.html', prod=prod, related=related)
    except TemplateNotFound:
        abort(404)


@marketplace.route('/products', methods=['GET', 'POST'])
@marketplace.route('/products/<int:page>', methods=['GET', 'POST'])
def products(page=1):
    #instantiate form
    form = SearchProductForm()
    states = States.query.all()
    form_sort = [(1, 'Most Recent'), (2, 'Lowest'), (3, 'Highest')]

    # categories
    cats = Categories.query.all()

    keyword = request.args.get('keyword')
    location = request.args.get('state')
    sort = request.args.get('sort')
    prod_categories = request.args.getlist('category')

    items = Products.query
    # product filtering
    if keyword:
        items = items.filter(Products.product_name.ilike('%' + keyword + '%'))
    if location:
        if location != str(1):
            items = items.join(Products.farm, aliased=True).filter_by(location_state=location)
    if prod_categories:
        items = items.filter(Products.category_id.in_(prod_categories))

    # product sorting
    if sort:
        if sort == '1':
            items = items.order_by(Products.date_posted.desc())
        elif sort == '2':
            items = items.order_by(Products.price.asc())
        else:
            items = items.order_by(Products.price.desc())
    else:
        items = items.order_by(Products.date_posted.desc())

    try:
        return render_template('marketplace/products.html',
                               prods=items.paginate(page, current_app.config['PRODUCTS_PER_PAGE']), form=form,
                               cats=cats, states=states, form_sort=form_sort)
    except TemplateNotFound:
        abort(404)


@marketplace.route('/messages', methods=['GET'])
@login_required
def messages():
    try:
        return render_template('marketplace/messages.html')
    except TemplateNotFound:
        abort(404)


@marketplace.route('/rating', methods=['GET'])
@login_required
def rating():
    try:
        return render_template('marketplace/rating.html')
    except TemplateNotFound:
        abort(404)

        
@marketplace.route('/send-message/<int:userid>', methods=['GET'])
@login_required
def send_message(userid):
    if current_user.id == userid:
        return redirect(url_for('.messages'))

    # check if actual user
    user = User.query.get(userid)

    if user is None:
        return redirect(url_for('.messages'))

    # check if message thread exists between both users
    thread = MessageThread.query.filter(or_(and_(MessageThread.sender_id == current_user.id,
                                                 MessageThread.receiver_id == userid),
                                            and_(MessageThread.sender_id == userid,
                                                 MessageThread.receiver_id == current_user.id)
                                            )
                                        ).one_or_none()
    if thread:
        return redirect(url_for('.conversation', thread_id=thread.urid))
    else:
        thread = MessageThread(current_user.id, userid)
        db.session.add(thread)
        db.session.commit()
        return redirect(url_for('.conversation', thread_id=thread.urid))


@marketplace.route('/conversation/<thread_id>', methods=['GET', 'POST'])
@login_required
def conversation(thread_id):
    # load thread
    thread = MessageThread.query.filter(MessageThread.urid == thread_id).one_or_none()
    # check if thread exists
    if thread:
        # check if user is entitled to thread
        if thread.sender_id == current_user.id or thread.receiver_id == current_user.id:
            # get other user
            if thread.sender_id == current_user.id:
                user = thread.receiver
            else:
                user = thread.sender

            # update read status for current user messages
            db.session.query(Message).filter(Message.receiver_id == current_user.id, Message.thread_id == thread.id,
                                     Message.read_status == 0).update({'read_status': 1, 'date_read': func.now()},
                                                                      synchronize_session=False)
            db.session.commit()

            form = MessageForm()
            if form.validate_on_submit():
                msg = Message(form.message.data, thread.id, current_user.id, user.id)
                db.session.add(msg)
                db.session.commit()
                return redirect(url_for('.conversation', thread_id=thread_id))
            # render form
            return render_template('marketplace/conversation.html', user=user, thread=thread, form=form)
        else:
            flash('This message thread does not exist', 'error')
            return redirect(url_for('.messages'))
    else:
        flash('This message thread does not exist', 'error')
        return redirect(url_for('.messages'))


@marketplace.route('/delete-product/<int:prod_id>', methods=['GET'])
@login_required
@is_farmer
def delete_product(prod_id):
    prod = Products.query.get(prod_id)

    if current_user.id == prod.farm.farmer.id:
        # remove all pictures from server
        for p in prod.pictures:
            remove_file(p.picture_name)
            #os.remove(os.path.join(current_app.static_folder, current_app.config['UPLOAD_FOLDER'], p.picture_name))
        db.session.delete(prod)
        db.session.commit()
        flash("The product has been successfully deleted", 'success')
        return redirect(url_for('.my_products'))
    else:
        abort(401)


@marketplace.route('/delete-picture/<int:pic_id>', methods=['GET'])
@login_required
@is_farmer
def delete_picture(pic_id):
    img = ProductPictures.query.get(pic_id)

    if current_user.id == img.product.farm_id:
        # get product id
        prod_id = img.product_id
        # get image
        remove_file(img.picture_name)
        #os.remove(os.path.join(current_app.static_folder, current_app.config['UPLOAD_FOLDER'], img.picture_name))
        db.session.delete(img)
        db.session.commit()
        flash("The picture has been deleted", 'success')
        return redirect(url_for('.view_product', prod_id=prod_id))
    else:
        abort(401)


@marketplace.route('/wishlist', methods=['GET'])
@login_required
def wishlist():
    try:
        return render_template('marketplace/wishlist.html')
    except TemplateNotFound:
        abort(404)


@marketplace.route('/wishlist/add-item', methods=['GET', 'POST'])
@login_required
def add_wishlist():
    form = AddWishlistForm()
    form.preferred_location.choices = [(a.id, a.state) for a in States.query.all()]

    if form.validate_on_submit():
        wish = Wishlist()

        wish.product_name = form.product_name.data
        wish.description = form.description.data
        wish.user_id = current_user.id
        wish.preferred_location = form.preferred_location.data

        db.session.add(wish)
        db.session.commit()
        flash("The item has been added to your wishlist", 'success')
        return redirect(url_for('.wishlist'))
    try:
        return render_template('marketplace/add_wishlist.html', form=form)
    except TemplateNotFound:
        abort(404)


@marketplace.route('/wishlist/remove-item/<int:wishid>', methods=['GET', 'POST'])
@login_required
def remove_wishlist(wishid):
    # load wish
    wish = Wishlist.query.get(wishid)

    # check that wish belongs to current user
    if current_user.id == wish.user_id:
        db.session.delete(wish)
        db.session.commit()
        flash("The wishlist item has been successfully removed.", 'success')
        return redirect(url_for('.wishlist'))
    else:
        flash("You can only remove items from your own wishlist", 'error')
        return redirect(url_for('.wishlist'))


@marketplace.route('/noticeboard', methods=['GET'])
@login_required
def noticeboard():
    if current_user.notice_views:
        # update users last notice view
        current_user.notice_views.date_viewed = func.now()
        db.session.add(current_user)
        db.session.commit()
    else:
        nview = NoticeViews()
        nview.user_id = current_user.id
        nview.date_viewed = func.now()
        db.session.add(nview)
        db.session.commit()

    notice = Notices.query.filter(Notices.status == 0).order_by(Notices.date_posted.desc()).first()
    noticelist = Notices.query.filter(Notices.status == 0).order_by(Notices.date_posted.desc()).limit(10).all()

    try:
        return render_template('marketplace/noticeboard.html', notice=notice, noticelist=noticelist)
    except TemplateNotFound:
        abort(404)


@marketplace.route('/noticeboard/view/<int:notice_id>', methods=['GET'])
@login_required
def notice_view(notice_id):
    # get notice
    notice = Notices.query.filter(Notices.id == notice_id).first_or_404()
    noticelist = Notices.query.filter(Notices.id != notice_id, Notices.status == 0).order_by(Notices.date_posted.desc()).limit(10).all()

    if notice:
        try:
            return render_template('marketplace/noticeboard.html', notice=notice, noticelist=noticelist)
        except TemplateNotFound:
            abort(404)


@marketplace.route('/noticeboard/archives', methods=['GET'])
@login_required
def notice_archive():
    noticelist = Notices.query.filter(Notices.status == 0).order_by(Notices.date_posted.desc()).all()

    try:
        return render_template('marketplace/notice_archives.html', noticelist=noticelist)
    except TemplateNotFound:
        abort(404)


# error handlers
@marketplace.errorhandler(404)
def page_not_found(e):
    return render_template('marketplace/404.html', title="Page Not Found", msg="Sorry, but the page you were trying to view does not exist."), 404