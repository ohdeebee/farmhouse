import os
import uuid
from flask import Blueprint, render_template, flash, redirect, url_for, current_app, request
from flask_login import login_user, logout_user, current_user
from werkzeug.utils import secure_filename
from functools import wraps
from sqlalchemy import func
from farm.util.image_util import compress_image
from farm import lm
from farm.models import db, User, Verification, Notices, Wishlist, ReportUser, Categories, FarmProfile, FeaturedFarms,\
    Products, FeaturedProducts, AuditTrail, Measures, ProductPictures
from farm.forms import AddAdminForm, LoginForm, AddPostForm, CategoryForm, MeasureForm, AddProductForm

admin = Blueprint('admin', __name__)
lm.blueprint_login_views = '.index'
operation_types = [(0, 'User Management'), (1, 'User Verification'), (2, 'Featured Products'), (3, 'Featured Farms'),
                   (4, 'Admin Management'), (5, 'Noticeboard Management'), (6, 'Wishlist Management'),
                   (7, 'Complaint Resolution'), (8, 'Category Management'), (9, 'Measures'), (10, 'Product Management')]
category_images = ['chicken-looking-right.png', 'pig.png', 'raw-fish.png', 'sheep.png', 'sprout.png', 'vegetables.png',
                   'snail.png', 'tractor.png',  'cow.png', 'watering-can.png', 'wheat.png']

# function to log user action
def log_action(operation_type, description):
    audit = AuditTrail()
    audit.user_id = current_user.id
    audit.type = operation_type
    audit.description = description

    db.session.add(audit)
    db.session.commit()


# remove uploaded files
def remove_file(file_name):
    os.remove(os.path.join(current_app.static_folder, current_app.config['UPLOAD_FOLDER'], file_name))


# decorator to determine if user is authenticated and is admin
def is_admin(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if current_user.is_authenticated:
            if not current_user.is_admin():
                flash('You are not authorized to access this page.', 'error')
                return redirect(url_for('.index'))
            return f(*args, **kwargs)
        else:
            flash('You have to login to access this page', 'error')
            return redirect(url_for('.index'))
    return decorated_function


@admin.route('/all-users', methods=['GET'])
@is_admin
def all_users():
    users = User.query.filter(User.user_type != 2).all()

    return render_template('admin/list_all_users.html', users=users)


@admin.route('/verified-users', methods=['GET'])
@is_admin
def verified_users():
    users = User.query.filter(User.verified.has(), User.user_type != 2).all()

    return render_template('admin/verified_users.html', users=users)


@admin.route('/all-farms', methods=['GET'])
@is_admin
def all_farms():
    farms = FarmProfile.query.all()
    return render_template('admin/list_farms.html', farms=farms)


@admin.route('/add-featured-farm/<int:farm_id>', methods=['GET'])
@is_admin
def add_featured_farm(farm_id):
    # count featured products
    feats = FeaturedFarms.query.all()

    if len(feats) >= current_app.config['FEATURED_FARM_COUNT']:
        flash('You cannot have more than ' + current_app.config['FEATURED_FARM_COUNT'] + ' featured farms', 'error')
        return redirect(url_for('.all_farms'))

    # check if farm exists
    farm = FarmProfile.query.get(farm_id)

    if not farm:
        flash('This farm does not exist', 'error')
        return redirect(url_for('.all_farms'))

    # check if farm is already featured
    # if farm.product_count < 4:
    #     flash('Farms must have at least 4 products to be featured', 'error')
    #     return redirect(url_for('.all_farms'))

    # check if farm is already featured
    if farm.is_featured:
        flash('This farm is already featured', 'error')
        return redirect(url_for('.all_farms'))

    # add to featured list
    ft = FeaturedFarms()
    ft.farm_id = farm.id
    db.session.add(ft)
    db.session.commit()

    # log action
    log_action(operation_types[3][1], "Added new featured farm: {0}".format(farm))

    flash('The farm has been successfully added to the list of featured farms', 'success')
    return redirect(url_for('.all_farms'))


@admin.route('/remove-featured-farm/<int:feat_id>', methods=['GET'])
@is_admin
def remove_featured_farm(feat_id):
    ft = FeaturedFarms.query.get(feat_id)

    if ft:
        # log action
        log_action(operation_types[3][1], "Removed featured farm: {0}".format(ft.farm))
        db.session.delete(ft)
        db.session.commit()

    flash('The farm has been removed from the list of featured farms', 'success')
    return redirect(url_for('.featured_farms'))


@admin.route('/featured-farms', methods=['GET'])
@is_admin
def featured_farms():
    ft = FeaturedFarms.query.all()

    return render_template('admin/featured_farms.html', feats=ft)


@admin.route('/all-products', methods=['GET'])
@is_admin
def all_products():
    prods = Products.query.all()
    return render_template('admin/list_products.html', prods=prods)


@admin.route('/edit-product/<int:prod_id>', methods=['GET', 'POST'])
@is_admin
def edit_product(prod_id):
    prod = Products.query.get(prod_id)
    form = AddProductForm(obj=prod)
    form.category.choices = [(a.id, a.category) for a in Categories.query.order_by('category')]
    form.measure.choices = [(a.id, a.measure) for a in Measures.query.order_by('measure')]

    if form.validate_on_submit():
        prod.product_name = form.product_name.data
        prod.description = form.description.data
        prod.price = form.price.data
        prod.measure_id = form.measure.data
        prod.category_id = form.category.data

        # for images
        images = request.files.iteritems()

        if images:
            for p in images:
                print(prod.picture_count())
                # check the amount of pictures product already has
                if prod.picture_count() >= current_app.config['MAX_PICTURE_COUNT']:
                    flash('You cannot upload more than '+ str(current_app.config['MAX_PICTURE_COUNT']) + ' pictures for a product. Delete pictures first')
                    break
                img = p[1]
                # Create Images
                if img.filename:
                    file_name = str(uuid.uuid4())[0:10] + secure_filename(img.filename)
                    image_file = os.path.join(current_app.static_folder, current_app.config['UPLOAD_FOLDER'], file_name)
                    img.save(image_file)
                    compress_image(image_file)

                    pic = ProductPictures()
                    pic.picture_name = file_name
                    prod.pictures.append(pic)
                    db.session.add(prod)
                    db.session.commit()

        db.session.add(prod)
        db.session.commit()
        flash('The product has been successfully updated', 'success')
        return redirect(url_for('.all_products'))

    return render_template('admin/edit_product.html', prod=prod, form=form)


@admin.route('/delete-picture/<int:pic_id>', methods=['GET'])
@is_admin
def delete_picture(pic_id):
    img = ProductPictures.query.get(pic_id)

    # get product id
    prod_id = img.product_id
    # get image
    remove_file(img.picture_name)

    db.session.delete(img)
    db.session.commit()

    # log action
    log_action(operation_types[10][1], "Removed product picture")

    flash("The picture has been deleted", 'success')
    return redirect(url_for('.edit_product', prod_id=prod_id))


@admin.route('/add-featured-product/<int:prod_id>', methods=['GET'])
@is_admin
def add_featured_product(prod_id):
    # count featured products
    feats = FeaturedProducts.query.all()

    if len(feats) >= current_app.config['FEATURED_PRODUCT_COUNT']:
        flash('You cannot have more than ' + current_app.config['FEATURED_PRODUCT_COUNT'] + ' featured products', 'error')
        return redirect(url_for('.all_products'))

    # check if farm exists
    prod = Products.query.get(prod_id)

    if not prod:
        flash('This product does not exist', 'error')
        return redirect(url_for('.all_products'))

    # check if farm is already featured
    if prod.is_featured:
        flash('This product is already featured', 'error')
        return redirect(url_for('.all_products'))

    # product needs to have picture
    if not prod.has_pictures():
        flash('Only products with pictures can be featured', 'error')
        return redirect(url_for('.all_products'))

    # add to featured list
    ft = FeaturedProducts()
    ft.product_id = prod.id
    db.session.add(ft)
    db.session.commit()

    # log action
    log_action(operation_types[2][1], "Added new featured product: {0}".format(prod))

    flash('The product has been successfully added to the list of featured products', 'success')
    return redirect(url_for('.all_products'))


@admin.route('/remove-featured-product/<int:feat_id>', methods=['GET'])
@is_admin
def remove_featured_product(feat_id):
    ft = FeaturedProducts.query.get(feat_id)

    if ft:
        # log action
        log_action(operation_types[2][1], "Removed featured product: {0}".format(ft.product))
        db.session.delete(ft)
        db.session.commit()

    flash('The product has been removed from the list of featured products', 'success')
    return redirect(url_for('.featured_products'))


@admin.route('/featured-products', methods=['GET'])
@is_admin
def featured_products():
    ft = FeaturedProducts.query.all()

    return render_template('admin/featured_products.html', feats=ft)


@admin.route('/manage-measurements', methods=['GET'])
@is_admin
def manage_measures():
    measures = Measures.query.all()

    return render_template('admin/measures.html', measures=measures)


@admin.route('/edit-measurement/<int:mid>', methods=['GET', 'POST'])
@is_admin
def edit_measure(mid):
    measure = Measures.query.get(mid)
    form = MeasureForm()

    if form.validate_on_submit():
        measure.measure = form.measure.data

        db.session.add(measure)
        db.session.commit()

        # log action
        log_action(operation_types[9][1], "Updated measurement: {0}".format(measure))

        flash('The item has been successfully updated', 'success')
        return redirect(url_for('.manage_measures'))

    return render_template('admin/add_measure.html', measure=measure, form=form)


@admin.route('/add-measurement', methods=['GET', 'POST'])
@is_admin
def add_measure():
    form = MeasureForm()

    if form.validate_on_submit():
        measure = Measures()
        measure.measure = form.measure.data

        db.session.add(measure)
        db.session.commit()

        # log action
        log_action(operation_types[9][1], "Added new measurement: {0}".format(measure))

        flash('The new item has been successfully created', 'success')
        return redirect(url_for('.manage_measures'))
    return render_template('admin/add_measure.html', form=form)


@admin.route('/manage-posts', methods=['GET'])
@is_admin
def manage_posts():
    notices = Notices.query.order_by(Notices.date_posted.desc()).all()

    return render_template('admin/notices.html', notices=notices)


@admin.route('/add-post', methods=['GET', 'POST'])
@is_admin
def add_post():
    form = AddPostForm()

    if form.validate_on_submit():
        notice = Notices()
        notice.title = form.title.data
        notice.notice = form.notice.data

        db.session.add(notice)
        db.session.commit()

        # log action
        log_action(operation_types[5][1], "Added new noticeboard post: {0}".format(notice))

        flash('Your post has been successfully saved to the noticeboard', 'success')
        return redirect(url_for('.manage_posts'))

    return render_template('admin/add_post.html', form=form)


@admin.route('/toggle-post-status/<int:operation>/<int:post_id>', methods=['GET'])
@is_admin
def toggle_post_status(operation, post_id):
    if operation not in (0, 1):
        flash('Invalid operation', 'error')
        return redirect(url_for('.all_users'))

    post = Notices.query.get(post_id)
    if not post:
        flash('This post does not exist', 'error')
        return redirect(url_for('.manage_posts'))

    post.status = operation
    db.session.add(post)
    db.session.commit()

    if operation == 0:
        # log action
        log_action(operation_types[5][1], "Published exisiting noticeboard post: {0}".format(post))
        flash('The post has been successfully published', 'success')
    else:
        # log action
        log_action(operation_types[5][1], "Archived noticeboard post: {0}".format(post))
        flash('The post has been successfully archived', 'success')
    return redirect(url_for('.manage_posts'))


@admin.route('/delete-post/<int:post_id>', methods=['GET'])
@is_admin
def delete_post(post_id):
    post = Notices.query.get(post_id)
    if not post:
        flash('This post does not exist', 'error')
        return redirect(url_for('.manage_posts'))

    # log action
    log_action(operation_types[5][1], "Deleted noticeboard post: {0}".format(post))

    db.session.delete(post)
    db.session.commit()
    flash('The post has been deleted', 'success')
    return redirect(url_for('.manage_posts'))


@admin.route('/toggle-user-status/<int:operation>/<int:user_id>', methods=['GET'])
@is_admin
def toggle_user_status(operation, user_id, complaint=False):
    if operation not in (0, 1):
        flash('Invalid operation', 'error')
        return redirect(url_for('.all_users'))

    user = User.query.get(user_id)
    if not user:
        flash('This user does not exist', 'error')
        return redirect(url_for('.all_users'))

    user.status = operation
    db.session.add(user)
    db.session.commit()

    if complaint:
        flash('The user\'s account has been deactivated', 'success')
        # log action
        log_action(operation_types[0][1], "Deactivated user account: {0}".format(user))
        return redirect(url_for('.manage_complaints'))

    if operation == 0:
        # log action
        log_action(operation_types[0][1], "Activated user account: {0}".format(user))
        flash('The user\'s account has been successfully activated', 'success')
    else:
        # log action
        log_action(operation_types[0][1], "Deactivated user account: {0}".format(user))
        flash('The user\'s account has been successfully deactivated', 'success')

    return redirect(url_for('.all_users'))


@admin.route('/verify/<int:user_id>', methods=['GET'])
@is_admin
def verify(user_id):
    user = User.query.get(user_id)

    if not user:
        flash('This user does not exist', 'error')
        return redirect(url_for('.all_users'))

    # check if user is already verified
    if user.is_verified():
        flash('This user is already verified', 'error')
        return redirect(url_for('.all_users'))

    ver = Verification()
    ver.user_id = user_id
    db.session.add(ver)
    db.session.commit()

    # log action
    log_action(operation_types[1][1], "Verified user account: {0}".format(user))

    flash('The user has been verified', 'success')
    return redirect(url_for('.all_users'))


@admin.route('/unverify/<int:user_id>', methods=['GET'])
@is_admin
def unverify(user_id):
    ver = Verification.query.filter(Verification.user_id == user_id).one_or_none()

    if not ver:
        flash('This user is not verified', 'error')
        return redirect(url_for('.verified_users'))

    # log action
    log_action(operation_types[1][1], "Unverified user account: {0}".format(ver.user))
    db.session.delete(ver)
    db.session.commit()
    flash('The user has been unverified', 'success')
    return redirect(url_for('.verified_users'))


@admin.route('/index', methods=['GET', 'POST'])
@admin.route('/', methods=['GET', 'POST'])
def index():
    # ensure user is not logged in
    if current_user.is_authenticated:
        logout_user()

    form = LoginForm()
    if form.validate_on_submit():
        adminuser = User.query.filter_by(username=form.username.data.lower()).one_or_none()

        if adminuser:
            if adminuser.status == 0:
                if adminuser.user_type == 2:
                    if adminuser.check_password(form.password.data):
                        login_user(adminuser)
                        flash('Welcome to the backend portal of theAgricStore', 'success')
                        return redirect(url_for('.all_users'))
                    else:
                        flash('Invalid login credentials', 'error')
                else:
                    flash('Your account is not authorized to view this page', 'error')
            else:
                flash('Your account has been deactivated', 'error')
        else:
            flash('Invalid login credentials', 'error')
    return render_template('admin/index.html', form=form)


@admin.route('/add-admin', methods=['GET', 'POST'])
@admin.route('/add-admin-user', methods=['GET', 'POST'])
@is_admin
def add_admin():
    form = AddAdminForm()

    if form.validate_on_submit():
        adminuser = User(form.full_name.data.strip(), form.username.data.strip(), form.email.data.strip(),
                         form.password.data, form.phone.data.strip(), form.gender.data, 2)
        db.session.add(adminuser)
        db.session.commit()

        # log action
        log_action(operation_types[4][1], "Created new admin account: {0}".format(adminuser))

        flash('The user has been successfully created', 'success')
        return redirect(url_for('.manage_admin'))
    return render_template('admin/add_admin.html', form=form)


@admin.route('/manage-administrators', methods=['GET'])
@is_admin
def manage_admin():
    admins = User.query.filter(User.user_type == 2).all()
    return render_template('admin/manage_admin.html', admins=admins)


@admin.route('/delete-administrator/<int:admin_id>', methods=['GET'])
@is_admin
def delete_admin(admin_id):
    if current_user.id == admin_id:
        flash('You cannot delete your own account', 'error')
        return redirect(url_for('.manage_admin'))

    adminuser = User.query.get(admin_id)
    if not adminuser:
        flash('This administrator does not exist', 'error')
        return redirect(url_for('.manage_admin'))

    # log action
    log_action(operation_types[4][1], "Deleted admin account: {0}".format(adminuser))

    db.session.delete(adminuser)
    db.session.commit()
    flash('The administrator has been deleted', 'success')
    return redirect(url_for('.manage_admin'))


@admin.route('/toggle-administrator-status/<int:operation>/<int:admin_id>', methods=['GET'])
@is_admin
def toggle_admin_status(operation, admin_id):
    if current_user.id == admin_id:
        flash('You cannot change your own account status', 'error')
        return redirect(url_for('.manage_admin'))

    if operation not in (0, 1):
        flash('Invalid operation', 'error')
        return redirect(url_for('.manage_admin'))

    adminuser = User.query.get(admin_id)
    if not adminuser:
        flash('This administrator does not exist', 'error')
        return redirect(url_for('.manage_admin'))

    adminuser.status = operation
    db.session.add(adminuser)
    db.session.commit()

    if operation == 0:
        # log action
        log_action(operation_types[4][1], "Activated admin account: {0}".format(adminuser))
        flash('The administrator\'s account has been successfully activated', 'success')
    else:
        # log action
        log_action(operation_types[4][1], "Deactivated admin account: {0}".format(adminuser))
        flash('The user\'s account has been successfully deactivated', 'success')
    return redirect(url_for('.manage_admin'))


@admin.route('/manage-wishlist', methods=['GET'])
@is_admin
def manage_wishlist():
    items = Wishlist.query.order_by(Wishlist.date_posted.desc()).all()
    return render_template('admin/manage_wishlist.html', items=items)


@admin.route('/toggle-item-status/<int:operation>/<int:wish_id>', methods=['GET'])
@is_admin
def toggle_item_status(operation, wish_id):
    if operation not in (0, 1):
        flash('Invalid operation', 'error')
        return redirect(url_for('.manage_wishlist'))

    wish = Wishlist.query.get(wish_id)
    if not wish:
        flash('This item does not exist', 'error')
        return redirect(url_for('.manage_wishlist'))

    wish.status = operation
    if operation == 1:
        wish.date_updated = func.now()
        # log action
        log_action(operation_types[6][1], "Treated wishlist request: {0} belonging to {1}".format(wish, wish.owner))
    else:
        wish.date_updated = None
        # log action
        log_action(operation_types[6][1], "Marked wishlist request as pending: {0} belonging to {1}".format(wish, wish.owner))

    db.session.add(wish)
    db.session.commit()

    flash('The item\'s status has been successfully updated', 'success')
    return redirect(url_for('.manage_wishlist'))


@admin.route('/delete-item/<int:wish_id>', methods=['GET'])
@is_admin
def delete_item(wish_id):
    wish = Wishlist.query.get(wish_id)
    if not wish:
        flash('This item does not exist', 'error')
        return redirect(url_for('.manage_wishlist'))

    # log action
    log_action(operation_types[6][1], "Deleted wishlist request: {0} belonging to {1}".format(wish, wish.owner))

    db.session.delete(wish)
    db.session.commit()
    flash('The item has been deleted', 'success')
    return redirect(url_for('.manage_wishlist'))


@admin.route('/manage-complaints', methods=['GET'])
@is_admin
def manage_complaints():
    items = ReportUser.query.order_by(ReportUser.date_posted.desc()).all()
    return render_template('admin/manage_complaints.html', items=items)


@admin.route('/audit-trail', methods=['GET'])
@is_admin
def audit_trail():
    items = AuditTrail.query.order_by(AuditTrail.date_created.desc()).all()
    return render_template('admin/audit_trail.html', items=items)


@admin.route('/toggle-complaint-status/<int:operation>/<int:complaint_id>', methods=['GET'])
@is_admin
def toggle_complaint_status(operation, complaint_id):
    if operation not in (0, 1):
        flash('Invalid operation', 'error')
        return redirect(url_for('.manage_complaints'))

    complaint = ReportUser.query.get(complaint_id)
    if not complaint:
        flash('This item does not exist', 'error')
        return redirect(url_for('.manage_complaints'))

    complaint.status = operation
    db.session.add(complaint)
    db.session.commit()

    if operation == 1:
        # log action
        log_action(operation_types[7][1], "Treated complaint from: {0}".format(complaint.reported_by))
        flash('The complaint has been treated', 'success')
    else:
        # log action
        log_action(operation_types[7][1], "Marked complaint from {0} as pending".format(complaint.reported_by))
        flash('The complaint has been marked open', 'success')
    return redirect(url_for('.manage_complaints'))


@admin.route('/delete-complaint/<int:complaint_id>', methods=['GET'])
@is_admin
def delete_complaint(complaint_id):
    complaint = ReportUser.query.get(complaint_id)
    if not complaint:
        flash('This item does not exist', 'error')
        return redirect(url_for('.manage_complaints'))

    # log action
    log_action(operation_types[7][1], "Deleted complaint made by {0}".format(complaint.reported_by))

    db.session.delete(complaint)
    db.session.commit()
    flash('The complaint has been deleted', 'success')
    return redirect(url_for('.manage_complaints'))


@admin.route('/manage-categories', methods=['GET'])
@is_admin
def manage_categories():
    cats = Categories.query.all()
    return render_template('admin/manage_categories.html', cats=cats)


@admin.route('/edit-category/<int:cat_id>', methods=['GET', 'POST'])
@is_admin
def edit_category(cat_id):
    cat = Categories.query.get(cat_id)
    if not cat:
        flash('This category does not exist', 'error')
        return redirect(url_for('.manage_categories'))

    form = CategoryForm()
    if form.validate_on_submit():
        cat.category = form.category.data
        cat.image = form.category_icon.data

        db.session.add(cat)
        db.session.commit()
        # log action
        log_action(operation_types[8][1], "Edited category: {0}".format(cat))
        flash('The category has been updated', 'success')
        return redirect(url_for('.manage_categories'))
    return render_template('admin/edit_category.html', form=form, cat=cat, cat_images=category_images)


@admin.route('/add-category', methods=['GET', 'POST'])
@is_admin
def add_category():
    form = CategoryForm()
    if form.validate_on_submit():
        cat = Categories()
        cat.category = form.category.data
        cat.image = form.category_icon.data

        db.session.add(cat)
        db.session.commit()

        # log action
        log_action(operation_types[8][1], "Added new category: {0}".format(cat))

        flash('The category has been created', 'success')
        return redirect(url_for('.manage_categories'))
    return render_template('admin/add_category.html', form=form, cat_images=category_images)


@admin.route('/logout', methods=['GET'])
@is_admin
def logout():
    if not current_user.is_authenticated:
        return redirect(url_for('.index'))

    logout_user()
    flash('You have been successfully logged out.', 'success')
    return redirect(url_for('.index'))